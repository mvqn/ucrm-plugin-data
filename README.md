# ucrm-plugin-data
This library is a helper package for interacting with the UCRM PostgreSQL database directly.
This package is not a Plugin itself, but will greatly help in the development of actual UCRM Plugins. 

## Installation
Install the latest version with
```bash
$ composer require mvqn/ucrm-plugin-data
```

## Basic Usage
```php
<?php

// COMING SOON
```

## Documentation
COMING SOON

## Third Party Packages
Third party handlers, formatters and processors are
[listed in the wiki](https://github.com/Seldaek/monolog/wiki/Third-Party-Packages). You
can also add your own there if you publish one.

## About

### Requirements
- This package will be maintained in step with the PHP version used by UCRM to ensure 100% compatibility.
- This package does not require any PHP extensions that are not already enabled in the default UCRM installation. 

### Related Packages
[ucrm-plugin-core](https://bitbucket.org/mvqn/ucrm-plugin-core)\
The core plugin package used by all UCRM Plugins developed internally.

[ucrm-plugin-rest](https://bitbucket.org/mvqn/ucrm-plugin-rest)\
Another plugin package used to access the UCRM REST API.

### Submitting bugs and feature requests
Bugs and feature request are tracked on [Bitbucket](https://bitbucket.org/mvqn/ucrm-plugin-data/issues)

### Author
Ryan Spaeth <[rspaeth@mvqn.net](mailto:rspaeth@mvqn.net)>

### License
Monolog is licensed under the MIT License - see the `LICENSE` file for details.

### Acknowledgements
Credit to the Ubiquiti Team for giving us the luxury of Plugins!
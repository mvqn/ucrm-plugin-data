<?php
declare(strict_types=1);

namespace UCRM\Data;

use UCRM\Data\Exceptions\ModelCollectionTypeException;



/**
 * Class ModelCollection
 *
 * A specialized collection for use in our simple ORM.
 *
 * @package UCRM\Data
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class ModelCollection
{
    /** @var Model[] $models The backing array to store the collection of Models. */
    protected $models = [];



    /**
     * ModelCollection constructor.
     *
     * @param Model[] $models An initial array of Models to store in this collection.
     * @throws ModelCollectionTypeException Throws an exception if any of the elements are not of type Model.
     */
    public function __construct(array $models = [])
    {
        // Loop through each provided array element to ensure type compatibility...
        foreach($models as $model)
        {
            if($model instanceof Model)
                $this->models[] = $model;
            else
                throw new ModelCollectionTypeException("ModelCollection expects only Model objects!");
        }
    }



    /**
     * Pushes a Model to the end of the ModelCollection.
     *
     * @param Model $model The Model to push.
     * @return ModelCollection The resulting ModelCollection.
     */
    public function push(Model $model): ModelCollection
    {
        array_push($this->models, $model);
        return $this;
    }

    /**
     * Pushes an array of Models to the end of the ModelCollection.
     *
     * @param Model[] $models The Models to push.
     * @return ModelCollection The resulting ModelCollection.
     * @throws ModelCollectionTypeException Throws an exception if any of the elements are not of type Model.
     */
    public function pushMany(array $models): ModelCollection
    {
        // Loop through each provided array element to ensure type compatibility...
        foreach($models as $model)
        {
            if($model instanceof Model)
                //$this->models[] = $model;
                array_push($this->models, $model);
            else
                throw new ModelCollectionTypeException("ModelCollection expects only Model objects!");
        }

        return $this;
    }



    /**
     * Pops a Model from the end of the ModelCollection.
     *
     * @return Model The Model that was popped from the end of the ModelCollection.
     */
    public function pop(): Model
    {
        $model = array_pop($this->models);
        return $model;
    }

    /**
     * Pops a specified number of Models from the end of the ModelCollection, in FILO order.
     *
     * @param int $count The number of Models to pop from the end of the ModelCollection.
     * @return ModelCollection Returns a new ModelCollection containing only the popped Models, in FILO order.
     * @throws ModelCollectionTypeException Throws an exception if any of the elements are not of type Model.
     * @throws \OutOfRangeException Throws an exception if the count is negative.
     */
    public function popMany(int $count): ModelCollection
    {
        if($count < 0)
            throw new \OutOfRangeException("'\$count' must be greater than or equal to 0.");

        if($count === 0)
            return new ModelCollection([]);

        if($count > count($this->models))
            return new ModelCollection($this->models);

        $collection = [];

        for($i = 0; $i < $count; $i++)
            $collection[] = array_pop($this->models);

        return new ModelCollection($collection);
    }



    /**
     * @return Model
     */
    public function shift(): Model
    {
        $model = array_shift($this->models);
        return $model;
    }

    public function shiftMany(int $count): ModelCollection
    {
        if($count < 0)
            throw new \OutOfRangeException("'\$count' must be greater than or equal to 0.");

        if($count === 0)
            return new ModelCollection([]);

        if($count > count($this->models))
            return new ModelCollection($this->models);

        $collection = [];

        for($i = 0; $i < $count; $i++)
            $collection[] = array_shift($this->models);

        return new ModelCollection($collection);
    }


    /**
     * Shifts a Model onto the front of a ModelCollection.
     *
     * @param Model $model The Model to shift.
     * @return ModelCollection Returns the resulting ModelCollection.
     */
    public function unshift(Model $model): ModelCollection
    {
        array_unshift($this->models, $model);
        return $this;
    }

    /**
     *
     *
     * @param array $models The array of Models to shift.
     * @return ModelCollection Returns the resulting ModelCollection.
     * @throws ModelCollectionTypeException Throws an exception if any of the elements are not of type Model.
     */
    public function unshiftMany(array $models): ModelCollection
    {
        // Loop through each provided array element to ensure type compatibility...
        foreach($models as $model)
        {
            if($model instanceof Model)
                array_unshift($this->models, $model);
            else
                throw new ModelCollectionTypeException("ModelCollection expects only Model objects!");
        }

        return $this;
    }


}
<?php
declare(strict_types=1);

namespace UCRM\Data;



/**
 * Class Model
 *
 * The base Model class for our simple ORM.
 *
 * @package UCRM\Data
 * @author  Ryan Spaeth <rspaeth@mvqn.net>
 */
abstract class Model
{
    /**
     * Model constructor.
     *
     * @param array $values An optional array of values for which to use to initialize this Model.
     */
    public function __construct(array $values = [])
    {
        // Loop through each KVP and store the contents in the Model object.
        foreach($values as $key => $value)
            $this->$key = $value;
    }

    /**
     * Overrides the default string representation of the class.
     *
     * @return string Returns a JSON representation of this Model.
     */
    public function __toString()
    {
        // Get an array of all Model properties.
        $assoc = get_object_vars($this);

        // Remove any that contain NULL values.
        $assoc = array_filter($assoc);

        // Return the array as a JSON string.
        return json_encode($assoc, JSON_UNESCAPED_SLASHES);
    }



    /**
     * Gets a Model from the database, given the specified ID.
     *
     * @param int $id The ID of the Model to query.
     * @return Model|null Returns a Model with the requested ID, otherwise NULL.
     * @throws Exceptions\DatabaseQueryException Throws an exception if the database connection is not valid.
     */
    public static function getById(int $id): ?Model
    {
        // Get information about the child class.
        $class = get_called_class();
        $table = $class::TABLE_NAME;
        $primary_key = $class::PRIMARY_KEY;

        // Execute a query against the database.
        $query = "SELECT * FROM $table WHERE $primary_key = $id";
        $results = Database::query($query);

        // IF no results were found, return NULL...
        if($results->rowCount() === 0)
            return null;

        /** @var Model $model */
        $model = new $class($results->fetch());

        // OTHERWISE, return the populated child Model.
        return $model;
    }

    /**
     * Gets an array of ALL Models from the database.
     *
     * @return Model[] Returns all of the Models from the appropriate table.
     * @throws Exceptions\DatabaseQueryException Throws an exception if the database connection is not valid.
     */
    public function select(): array
    {
        // Get information about the child class.
        $class = get_called_class();
        $table = $class::TABLE_NAME;

        // Execute a query against the database.
        $query = "SELECT * FROM $table";
        $results = Database::query($query);

        $models = [];

        // Add each resulting row to the array of Models...
        while($row = $results->fetch())
        {
            /** @var Model $model */
            $model = new $class($row);
            $models[] = $model;
        }

        // Finally, return the array of Models.
        return $models;
    }

    /**
     * Gets an array of ALL Models from the database, given the specified criteria.
     *
     * @param string $column The column on which to match.
     * @param string $value The value of which to match.
     * @return Model[] Returns all of the Models from the appropriate table, given the specified criteria.
     * @throws Exceptions\DatabaseQueryException Throws an exception if the database connection is not valid.
     */
    public static function where(string $column, string $value): array
    {
        // Get information about the child class.
        $class = get_called_class();
        $table = $class::TABLE_NAME;

        // Execute a query against the database.
        $query = "SELECT * FROM $table WHERE $column = '$value'";
        $results = Database::query($query);

        $models = [];

        // Add each resulting row to the array of Models...
        while($row = $results->fetch())
        {
            /** @var Model $model */
            $model = new $class($row);
            $models[] = $model;
        }

        // Finally, return the array of Models.
        return $models;
    }

    /**
     * Gets an array of ALL Models from the database, given the specified criteria.
     *
     * @param string $column The column on which to match.
     * @param string $pattern The pattern of which to match, using SQL LIKE syntax.
     * @return Model[] Returns all of the Models from the appropriate table, given the specified criteria.
     * @throws Exceptions\DatabaseQueryException Throws an exception if the database connection is not valid.
     */
    public static function like(string $column, string $pattern): array
    {
        // Get information about the child class.
        $class = get_called_class();
        $table = $class::TABLE_NAME;

        // Execute a query against the database.
        $query = "SELECT * FROM $table WHERE $column LIKE '$pattern'";
        $results = Database::query($query);

        $models = [];

        // Add each resulting row to the array of Models...
        while($row = $results->fetch())
        {
            /** @var Model $model */
            $model = new $class($row);
            $models[] = $model;
        }

        // Finally, return the array of Models.
        return $models;
    }



    // TODO: Add Model->save() functionality?

}